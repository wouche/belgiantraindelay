function validateTime(inputText)
	{
		if (inputText === '') { 
			return true;
		}
	
		var timeformat = /^^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;
		// Match the date format through regular expression
		if(!inputText.match(timeformat))
		{
			var elements = document.getElementsByClassName("required");
			var i;
			for (i = 0; i < elements.length; i++) {
			  elements[i].innerHTML=""
			} 
			document.getElementById("depTimeError2").innerHTML="Use format HH:mm";
			return false;
		}
	}


