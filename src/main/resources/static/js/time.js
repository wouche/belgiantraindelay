function getTimeDifference(timeString1, timeString2) {
	var d1 = new Date();
	var [hours1,minutes1,seconds1] = timeString1.split(':'); 
	d1.setHours(hours1)
	d1.setMinutes(minutes1);
	d1.setSeconds(seconds1);
	
	var d2 = new Date();
	var [hours2,minutes2,seconds2] = timeString2.split(':'); 
	d2.setHours(hours2)
	d2.setMinutes(minutes2);
	d2.setSeconds(seconds2);
	
	var difference = d2-d1;
	var resultInMinutes = Math.round(difference / 60000);
    return resultInMinutes;
}