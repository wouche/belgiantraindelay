var autocompleteOptions = {				
	        source: function (request, response) {
	            $.getJSON("/api/search", {
	                input: request.term
	            }, response);
	        },
	        minLength: 2  	    
};


(function() {
	$("#startstation").autocomplete(autocompleteOptions);	
  })();

(function() {
	$("#stopstation").autocomplete(autocompleteOptions);	
  })();

(function() {
$('body').on('click', '[id^="intermediateStations"]', function() {
    $(this).autocomplete(autocompleteOptions);
});
})();


  