function addStationRow() {
	  const table = document.getElementById('stationsTable');
	  var rowindex = table.rows.length-1	  
	  var row = table.insertRow(rowindex);
	  var cell1 = row.insertCell(0);
	  var cell2 = row.insertCell(1);
	  
	  cell1.innerHTML = "Intermediate Station " + (rowindex-2);
	  cell2.innerHTML = '<input id="intermediateStations' + (rowindex-3) + '" data-stationindex="'+ (rowindex-3) + '" class="form-control mb-2 col-4 floated" type="text" name="intermediateStations[' + (rowindex-3) + ']" value="" placeholder="Intermediate Station"> <input class="floated" width="35" type="image" src="/img/remove.png" title="Remove Intermediate Station" onclick="removeStationRow(this)">';
	  
	}


function removeStationRow(currElement) {
	const table = document.getElementById('stationsTable');
	
    var parentRowIndex = currElement.parentNode.parentNode.rowIndex;
    table.deleteRow(parentRowIndex);
    
    // Update indexes of other intermediate station rows
    for (var i = 3; i < table.rows.length-1; i++) 
	  {
    	 var row = table.rows[i];
	     var firstCol = row.cells[0];
	     firstCol.innerText = "Intermediate Station " + (i-2);
	     var secondCol = row.cells[1];
	     secondCol.childNodes[0].setAttribute("id", "intermediateStations[" + (i-3) + "]");
	     secondCol.childNodes[0].setAttribute("data-stationindex", (i-2));
	     secondCol.childNodes[0].setAttribute("name", "intermediateStations[" + (i-3) + "]");
	     	     
	  }
}


