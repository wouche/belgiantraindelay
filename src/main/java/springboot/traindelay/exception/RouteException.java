package springboot.traindelay.exception;

/*
 * Exception thrown in case of a problem with the route calculation
 */
public class RouteException extends Exception {

	private static final long serialVersionUID = 1L;

	public RouteException() {
	}
	
	public RouteException(String errorMessage) {
        super(errorMessage);
    }

}
