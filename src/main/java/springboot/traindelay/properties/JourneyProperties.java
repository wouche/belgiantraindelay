package springboot.traindelay.properties;

import java.time.Duration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties("journeyproperty")
@Configuration
public class JourneyProperties {

	/*
	 * Minimum amount of minutes you need to transfer between two different trains at a station
	 */
	private Duration minimumTransferTime;

	public Duration getMinimumTransferTime() {
		return minimumTransferTime;
	}

	public void setMinimumTransferTime(Duration minimumTransferTime) {
		this.minimumTransferTime = minimumTransferTime;
	}

	
	
	
	

}
