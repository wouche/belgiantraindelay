package springboot.traindelay.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties("external-api-property")
@Configuration
public class ExternalAPIProperties {

	private String openDataUrl;
	
	private String iRailUrl;
	
	public String getOpenDataUrl() {
		return openDataUrl;
	}

	public void setOpenDataUrl(String openDataUrl) {
		this.openDataUrl = openDataUrl;
	}

	public String getIRailUrl() {
		return iRailUrl;
	}

	public void setIRailUrl(String iRailUrl) {
		this.iRailUrl = iRailUrl;
	}
	
	

}
