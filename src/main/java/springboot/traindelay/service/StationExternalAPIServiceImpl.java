package springboot.traindelay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springboot.traindelay.entity.StationData;
import springboot.traindelay.properties.ExternalAPIProperties;

@Service
public class StationExternalAPIServiceImpl extends ExternalAPIServiceImpl<StationData> {
	
	
	@Autowired
	public StationExternalAPIServiceImpl(ExternalAPIProperties theExternalAPIProperties) {
	
		this.url = theExternalAPIProperties.getIRailUrl();
		this.typeParameterClass = StationData.class;
	
	}

	
}
