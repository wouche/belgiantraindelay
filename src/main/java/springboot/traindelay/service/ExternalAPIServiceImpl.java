package springboot.traindelay.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ExternalAPIServiceImpl<T> implements ExternalAPIService<T> {
	
	@Autowired
	private WebClient.Builder webClientBuilder;
		 
	protected String url;
	
	protected Class<T> typeParameterClass;	
	
	public ExternalAPIServiceImpl() {		
		
	}
	
	
	@Override
	public T getData(HashMap<String, String> parameters) {
		
		String queryURL = buildQueryURL(parameters, url);
				
				// raise max in memory size buffer
				T data = webClientBuilder
					.exchangeStrategies(ExchangeStrategies.builder()
						    .codecs(configurer -> configurer
						      .defaultCodecs()
						      .maxInMemorySize(16 * 1024 * 1024))
						    .build())
					.build()
					.get()
					.uri(queryURL)
					.retrieve()
					.bodyToMono(typeParameterClass)
					.block();
				
				
				return data;	
	}

	
	private String buildQueryURL(HashMap<String, String> parameters, String url){
			
			String parametersString = parameters.entrySet().stream()
				    .map(p -> p.getKey() + "=" + p.getValue())
				    .reduce((p1, p2) -> p1 + "&" + p2)
				    .map(s -> "?" + s)
				    .orElse("");
			
			return url + parametersString;
		}



	
	
}
