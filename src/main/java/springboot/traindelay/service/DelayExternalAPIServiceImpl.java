package springboot.traindelay.service;

import org.springframework.stereotype.Service;

import springboot.traindelay.entity.DelayData;
import springboot.traindelay.properties.ExternalAPIProperties;

@Service
public class DelayExternalAPIServiceImpl extends ExternalAPIServiceImpl<DelayData>{

	public DelayExternalAPIServiceImpl(ExternalAPIProperties theExternalAPIProperties) {
		this.typeParameterClass = DelayData.class;
		this.url = theExternalAPIProperties.getOpenDataUrl();
	}

}
