package springboot.traindelay.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springboot.traindelay.entity.StationData;

@Service
public class StationServiceImpl implements StationService {

	private ExternalAPIService<StationData> stationAPIService;
	
	private List<String> stations;
	
	@Autowired
	public StationServiceImpl(StationExternalAPIServiceImpl theStationExternalAPIServiceImpl) {
		stationAPIService = theStationExternalAPIServiceImpl;
		
	}

	@Override
	public List<String> getStations() {
		if (this.stations == null){
			HashMap<String, String> parameters = new HashMap<String, String>();
			
			parameters.put("format","json");
			parameters.put("lang", "nl");
			
			StationData stationData = stationAPIService.getData(parameters);
			
			List<String> stationsResult = stationData.getStations().stream().map(record -> record.getName().toUpperCase()).collect(Collectors.toList());
		
			// Sort Alphabetically
			Collections.sort(stationsResult);
			
			this.stations = stationsResult;
			
		}
		
		return this.stations;	
	}
	
	@Override
	public List<String> searchStations(String input){
		
		this.getStations();
		return stations.stream().filter(s -> s.contains(input.toUpperCase())).collect(Collectors.toList());
		
	}
	
	@Override
	public boolean isStation(String station){
		
		this.getStations();
		return this.stations.contains(station.toUpperCase());
		
	}

}
