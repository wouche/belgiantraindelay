package springboot.traindelay.service;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springboot.traindelay.entity.DelayData;
import springboot.traindelay.entity.DelayRecord;
import springboot.traindelay.entity.Journey;
import springboot.traindelay.entity.JourneyRequest;
import springboot.traindelay.entity.Trip;
import springboot.traindelay.exception.RouteException;
import springboot.traindelay.properties.JourneyProperties;


@Service
public class TrainTimingServiceImpl implements TrainTimingService {

	private ExternalAPIService<DelayData> delayAPIService;
	
    private JourneyProperties journeyProperties;	 
    	 
	
	@Autowired
	public TrainTimingServiceImpl(JourneyProperties theJourneyProperties, DelayExternalAPIServiceImpl theDelayExternalAPIServiceImpl) {
		journeyProperties = theJourneyProperties;
		
		delayAPIService = theDelayExternalAPIServiceImpl;
	}

	
		
	@Override
	public Journey getJourney(JourneyRequest theJourneyRequest, Boolean realTime) throws RouteException {	
		
		Journey journey = new Journey();
				
		LocalTime startTime = theJourneyRequest.getStartTime();
		
		List<String> stations = new ArrayList<>(theJourneyRequest.getIntermediateStations());
		stations.add(0, theJourneyRequest.getStartStation());
		stations.add(theJourneyRequest.getStopStation());
		
		// TODO: get cleaner fix for this
		// To be able to calculate starttime of train, you need to get stoptime of previous train at the station
		String timeArrivalField;
		
		if (realTime) {
			timeArrivalField = "real_time_arr";
		}
		else {
			timeArrivalField = "planned_time_arr";
		}
		
		
		for(int i = 0; i < stations.size() - 1; i++ ){
			String startStation = stations.get(i);
			String stopStation = stations.get(i+1);
						
			Trip trip = this.getTrip(startTime, startStation, stopStation, realTime);
			journey.getTrips().add(trip);
					
			if (i < stations.size() - 2){
				startTime = LocalTime.parse(trip.getStopRecord().getFields().get(timeArrivalField), DateTimeFormatter.ISO_LOCAL_TIME).plus(this.journeyProperties.getMinimumTransferTime());			
			}
		}
		
		
		return journey;	
		
	}
	


	private Trip getTrip(LocalTime startTime, String startStation, String stopStation, Boolean realTime) throws RouteException {
		
		// Make distinction between theoretical time and real time
		String sortStartParameter = "";
		String sortStopParameter = "";
		
		if (realTime) {
			sortStartParameter = "real_time_dep";
			sortStopParameter = "real_time_arr";
		}
		else {
			sortStartParameter = "planned_time_dep";
			sortStopParameter = "planned_time_arr";
		}
				
		
		List<DelayRecord> startStationRecords = getFilteredStationRecords(startTime, startStation, sortStartParameter);
							
		List<DelayRecord> stopStationRecords = getFilteredStationRecords(startTime, stopStation, sortStopParameter);
		
		if (startStationRecords.isEmpty() || stopStationRecords.isEmpty()){
			throw new RouteException("No start or stop station records found");
		}
		
		Trip trip = createTrip(startStationRecords, stopStationRecords, sortStartParameter, sortStopParameter);
		
		return trip;
		
	}
	
	
	
	private List<DelayRecord> getFilteredStationRecords(LocalTime startTime, String station, String sortParameter){
		// Get data of station
		HashMap<String, String> parameters = new HashMap<String, String>();
		
		parameters.put("dataset","ruwe-gegevens-van-stiptheid-d-1");
		parameters.put("refine.ptcar_lg_nm_nl", station);
		parameters.put("sort",sortParameter);
		parameters.put("rows", "10000");
		
		DelayData trainDataStation = delayAPIService.getData(parameters);			
		List<DelayRecord> stationRecords = trainDataStation.getRecords();
		
		//Filter out  station records with departure/arrival time earlier then starttime or without departure/arrival time
		Predicate<DelayRecord> byTime = record -> LocalTime.parse(record.getFields().get(sortParameter), DateTimeFormatter.ISO_LOCAL_TIME).compareTo(startTime) >=0;
		Predicate<DelayRecord> removeNullTime = record -> record.getFields().get(sortParameter) != null;
			
		List<DelayRecord> filteredStationRecords = stationRecords.stream().filter(removeNullTime).filter(byTime).collect(Collectors.toList());
	
		return filteredStationRecords;	
	}
	
	

	// Create trip by getting stoprecord with earliest arrival time, which has a trainnumber that is also present in the startstationrecords.
	// Also check that arrival time in stop station is later then departure time in start station
	private Trip createTrip(List<DelayRecord> startStationRecords, List<DelayRecord> stopStationRecords, String sortStartParameter, String sortStopParameter) throws RouteException{
		for (DelayRecord stopRecord : stopStationRecords){
			String trainNumber = stopRecord.getFields().get("train_no");
			LocalTime arrivalTime = LocalTime.parse(stopRecord.getFields().get(sortStopParameter),DateTimeFormatter.ISO_LOCAL_TIME);
			
			// Predicate to check if train numbers match
			Predicate<DelayRecord> byTrainNumber = record -> record.getFields().get("train_no").equals(trainNumber);
			
			// Predicate to check if arrival time is after departure time
			Predicate<DelayRecord> byTime = startRecord -> LocalTime.parse(startRecord.getFields().get(sortStartParameter), DateTimeFormatter.ISO_LOCAL_TIME).compareTo(arrivalTime) < 0;
			
		    List<DelayRecord> startRecords = startStationRecords.stream().filter(byTrainNumber).filter(byTime).collect(Collectors.toList());
			if (startRecords.size() == 1){
				return new Trip(startRecords.get(0), stopRecord);
			}
			if (startRecords.size() > 1){
				throw new IllegalArgumentException("Error while creating trip");
			}
		}	
	
		throw new RouteException("No trip found");
	}


// TODO DTO object? Make this cleaner
	@Override
	public void addTimeDifferences(List<Trip> plannedTrips, List<Trip> realTimeTrips) {
		
		// Check if PlannedTrips and RealTimeTrips have same length
		if (plannedTrips.size() == realTimeTrips.size()){
			for (int i=0; i < plannedTrips.size(); i++){
				Trip plannedTrip = plannedTrips.get(i);
				Trip realTimeTrip = realTimeTrips.get(i);
				
				String plannedTimeDep = plannedTrip.getStartRecord().getFields().get("planned_time_dep");
				String realTimeDep = realTimeTrip.getStartRecord().getFields().get("real_time_dep");
				String timeDifferenceDep = this.getTimeDifference(plannedTimeDep, realTimeDep);
				realTimeTrip.getStartRecord().getFields().put("time_difference_dep", timeDifferenceDep);

				String plannedTimeArr = plannedTrip.getStopRecord().getFields().get("planned_time_arr");
				String realTimeArr = realTimeTrip.getStopRecord().getFields().get("real_time_arr");
				String timeDifferenceArr = this.getTimeDifference(plannedTimeArr, realTimeArr);
				realTimeTrip.getStopRecord().getFields().put("time_difference_arr", timeDifferenceArr);
				
			}
		}
		
		else{
			throw new IllegalArgumentException();
		}
		
	}
	
	/*
	 * Get time difference in minutes
	 */
	private String getTimeDifference(String time1, String time2){
		LocalTime localTime1 = LocalTime.parse(time1);
		LocalTime localTime2 = LocalTime.parse(time2);
		
		Long timeDifference = Duration.between(localTime1, localTime2).toMinutes();
		return Long.toString(timeDifference);
		
	}
	 

}
