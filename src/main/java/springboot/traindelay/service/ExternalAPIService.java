package springboot.traindelay.service;


import java.util.HashMap;

public interface ExternalAPIService<T> {
	
	public T getData(HashMap<String, String> parameters);	


}
