package springboot.traindelay.service;


import java.util.List;

import springboot.traindelay.entity.Journey;
import springboot.traindelay.entity.JourneyRequest;
import springboot.traindelay.entity.Trip;
import springboot.traindelay.exception.RouteException;

public interface TrainTimingService {
	
	public Journey getJourney(JourneyRequest journeyRequest, Boolean realTime) throws RouteException;
	
	public void addTimeDifferences(List<Trip> PlannedTrips, List<Trip> RealTimeTrips);
}
