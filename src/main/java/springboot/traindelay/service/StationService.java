package springboot.traindelay.service;

import java.util.List;

public interface StationService {
	
	public List<String> getStations();
	
	public List<String> searchStations(String input);

	public boolean isStation(String station);
}
