package springboot.traindelay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import springboot.traindelay.service.StationService;

@RestController
@RequestMapping("/api")
public class TrainRestController {

	StationService stationService;
	
	@Autowired
	public TrainRestController(StationService theStationService) {

		this.stationService = theStationService;
	}
	
	@GetMapping("/search")
	public @ResponseBody List<String> searchStations(@RequestParam String input){
		return this.stationService.searchStations(input);
	}

}
