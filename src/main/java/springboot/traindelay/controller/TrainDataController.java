package springboot.traindelay.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import springboot.traindelay.entity.Journey;
import springboot.traindelay.entity.JourneyRequest;
import springboot.traindelay.exception.RouteException;
import springboot.traindelay.service.TrainTimingService;

@Controller
@RequestMapping("/traindata")
public class TrainDataController {
	
	private TrainTimingService trainTimingService;
	

	public TrainDataController(TrainTimingService theTrainTimingService) {
		trainTimingService = theTrainTimingService;
	}
		
	

	@GetMapping("/formRouteInformation")
	public String showFormForAdd(Model theModel){
		
	
		// create model attribute to bind form data
		JourneyRequest theJourneyRequest = new JourneyRequest();
		
		theModel.addAttribute("journeyRequest", theJourneyRequest);
		
		return "traindelay/delay-form";
	}
	
	@PostMapping(path="/formUpdateRouteInformation", params={"updateJourneyRequest"})
	public String showFormForUpdate(@ModelAttribute("journeyRequest") JourneyRequest theJourneyRequest, Model theModel){
	
		theModel.addAttribute("journeyRequest", theJourneyRequest);
		
		return "traindelay/delay-form";
	}
	
	@PostMapping(path="/getDelayInformation", params={"createJourney"})
	public String createJourney(@Valid @ModelAttribute("journeyRequest") JourneyRequest theJourneyRequest, BindingResult theBindingResult, Model theModel){
				
		if (theBindingResult.hasErrors()) {
			System.out.println(theBindingResult);
			
			return "traindelay/delay-form";
		}
		
		// change all intermediate stations to uppercase format and trim them
		theJourneyRequest.getIntermediateStations().replaceAll(String::toUpperCase);
		theJourneyRequest.getIntermediateStations().replaceAll(String::trim);
		
		Journey realTimeJourney = new Journey();
		Journey plannedTimeJourney = new Journey();
		
		try {
			realTimeJourney = trainTimingService.getJourney(theJourneyRequest, true);
			plannedTimeJourney = trainTimingService.getJourney(theJourneyRequest, false);
		} catch (RouteException e) {
			e.printStackTrace();
			return "traindelay/route-error";
		}
				
		trainTimingService.addTimeDifferences(plannedTimeJourney.getTrips(), realTimeJourney.getTrips());
		
		
		theModel.addAttribute("realTimeTrips", realTimeJourney.getTrips());
		theModel.addAttribute("plannedTimeTrips", plannedTimeJourney.getTrips());
		
			
		// use a redirect to prevent dubplicate submissions
		return "traindelay/delay-information";
	}
	
	
	

}
