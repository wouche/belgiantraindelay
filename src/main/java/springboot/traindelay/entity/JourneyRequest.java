package springboot.traindelay.entity;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;


public class JourneyRequest {

	@NotNull(message="Required field")
    @DateTimeFormat(pattern = "HH:mm")
	private LocalTime startTime;
	
	@ValidStation
	private String startStation;
	
	@ValidStation
	private String stopStation; 	
	
	private List<@ValidStation String> intermediateStations = new ArrayList<> ();

	
	public JourneyRequest() {
	}
	

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public String getStartStation() {
		return startStation;
	}

	public void setStartStation(String startStation) {
		this.startStation = startStation.toUpperCase().trim();
	}

	public String getStopStation() {
		return stopStation;
	}

	public void setStopStation(String stopStation) {
		this.stopStation = stopStation.toUpperCase().trim();
	}
		

	public List<String> getIntermediateStations() {
		return intermediateStations;
	}
	
	

	@Override
	public String toString() {
		return "JourneyRequest [startTime=" + startTime + ", startStation=" + startStation + ", stopStation="
				+ stopStation + ", intermediateStations=" + intermediateStations + "]";
	}
	
	

}
