package springboot.traindelay.entity;

import java.util.List;


import com.fasterxml.jackson.annotation.JsonProperty;


public class DelayData {

	@JsonProperty
	private List<DelayRecord> delayRecords;
	
	@JsonProperty
	private Object parameters;
		
	public DelayData() {
		
	}

	public List<DelayRecord> getRecords() {
		return delayRecords;
	}

	public void setRecords(List<DelayRecord> delayRecords) {
		this.delayRecords = delayRecords;
	}

	public Object getParameters() {
		return parameters;
	}

	public void setParameters(Object parameters) {
		this.parameters = parameters;
	}
	
	
	
	

}
