package springboot.traindelay.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StationData {
	
	@JsonProperty("station")
	private List<StationRecord> stations;	
	

	public StationData() {
	}


	public List<StationRecord> getStations() {
		return this.stations;
	}


	public void setStations(List<StationRecord> stations) {
		this.stations = stations;
	}
			

	

}
