package springboot.traindelay.entity;

import java.util.ArrayList;
import java.util.List;

public class Journey {
	
	private List<Trip> trips = new ArrayList<> ();

	public Journey() {
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	@Override
	public String toString() {
		return "Journey [trips=" + trips + "]";
	}
	

}
