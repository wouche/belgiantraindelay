package springboot.traindelay.entity;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DelayRecord {
	
	@JsonProperty
	private Map<String, String> fields;
	
	@JsonProperty
	private String recordid;
		

	public DelayRecord() {
	}

	public Map<String, String> getFields() {
		return fields;
	}

	public void setFields(Map<String, String> fields) {
		this.fields = fields;
	}
	
	
	@Override
	public String toString() {
		return "Record [fields=" + fields + "]";
	}
	
	

}
