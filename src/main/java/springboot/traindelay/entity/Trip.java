package springboot.traindelay.entity;

public class Trip {

	private DelayRecord startRecord;
	
	private DelayRecord stopRecord;
	
	public Trip() {
	}
	
	public Trip(DelayRecord theStartRecord, DelayRecord theStopRecord) {
		startRecord = theStartRecord;
		stopRecord = theStopRecord;
	}

	public DelayRecord getStartRecord() {
		return startRecord;
	}

	public void setStartRecord(DelayRecord startRecord) {
		this.startRecord = startRecord;
	}

	public DelayRecord getStopRecord() {
		return stopRecord;
	}

	public void setStopRecord(DelayRecord stopRecord) {
		this.stopRecord = stopRecord;
	}

	@Override
	public String toString() {
		return "Trip [startRecord=" + startRecord + ", stopRecord=" + stopRecord + "]";
	}
	
	

}
