package springboot.traindelay.entity;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import springboot.traindelay.service.StationService;

public class StationValidator implements ConstraintValidator<ValidStation, String> {

	private StationService stationService;
	
	private String messageRequired;
	private String messageUnknown;
	
	@Autowired
	public StationValidator(StationService theStationService) {
		this.stationService = theStationService;
	}
	
	
	@Override
    public void initialize(ValidStation station) {
        this.messageRequired = station.messageRequired();
        this.messageUnknown = station.messageUnknown();
    }

	@Override
	public boolean isValid(String station, ConstraintValidatorContext context) {
		if (station == null){
			context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(this.messageRequired).addConstraintViolation();
			return false;
		}
		else if (station.length() < 1){
			context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(this.messageRequired).addConstraintViolation();
			return false;
		}
		
		context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(this.messageUnknown).addConstraintViolation();
		return this.stationService.isStation(station.trim());
	}

}
