package springboot.traindelay.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StationRecord {

	@JsonProperty
	private String name;
	
	
	public StationRecord() {
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "StationRecord [name=" + name + "]";
	}
	
	

}
